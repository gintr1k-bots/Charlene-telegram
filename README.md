# Sharaga-Changes

## Quick start
First, install all requirements:
```
pip install -r requirements.txt
```
Second, edit your `config.sample.py` and rename it to `config.py`.

Then you can start your bot via command `python main.py`.
