import time
import traceback
import threading
import asyncio
import cherrypy
import pymysql
import telebot

import config
import lib.lib as lib
from utils import pymysqlpool

# Setup telegram bot
bot = telebot.TeleBot(config.token)

# Set proxy connection if not empty
if not config.proxy_link:
    telebot.apihelper.proxy = {'https': config.proxy_link}

# WebhookServer, process webhook calls
class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                'content-type' in cherrypy.request.headers and \
                cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)

            # Get requests
            bot.process_new_updates([update])

            return ''
        else:
            raise cherrypy.HTTPError(403)

shedule = {
    'tmp': lib.SharagaShedule(url=config.tmp_file,
                              header_name=config.http_header,
                              human_format=config.human_datetime_format),

    'perm': lib.SharagaShedule(url=config.perm_file,
                               header_name=config.http_header,
                               human_format=config.human_datetime_format)
}

def create_conn():
    """ Функция для создания пула соединений с MySQL """

    return pymysql.connect(**config.database,
                           autocommit=True)

def shedule_text(*, header, link, updated, checked):
    return  f'*{header}* ([Загрузить]({link}))\n' + \
            f'Опубликовано: `{updated}`\n' + \
            f'Проверено: `{checked}`\n'

def message_shedule_body(shedule):
    return  shedule_text(header='Изменения в расписании',
                         link=shedule.get("tmp").url,
                         updated=shedule.get("tmp").human_datetime,
                         checked=shedule.get("tmp").last_check) + '\n' +\
            shedule_text(header='Постоянное расписание',
                         link=shedule.get("perm").url,
                         updated=shedule.get("perm").human_datetime,
                         checked=shedule.get("perm").last_check)

async def shedule_task():
    while True:
        last_temp_time = shedule.get("tmp").human_datetime
        last_perm_time = shedule.get("perm").human_datetime

        tmp_changes = False
        perm_changes = False

        if last_temp_time != shedule.get("tmp").get_changes():
            tmp_changes = True

        if last_perm_time != shedule.get("perm").get_changes():
            perm_changes = True

        if tmp_changes or perm_changes:
            message = '*Появились изменения в отслеживаемых файлах:*\n' +\
                      (f'Постоянное расписание (`{shedule.get("perm").human_datetime}`)\n' if \
                            perm_changes else '') +\
                      (f'Замены в расписании (`{shedule.get("tmp").human_datetime}`)\n' if \
                            tmp_changes else '')

            bot.send_message(config.admins[0], message,
                             parse_mode='Markdown',
                             disable_web_page_preview=True)

        await asyncio.sleep(15)

@bot.message_handler(commands=['force'])
def force_update(message):
    bot.send_chat_action(message.chat.id, 'typing')

    if message.from_user.id not in config.admins:
        bot.reply_to(message, '*Только администратор имеет право использовать данную команду.*')
        return

    shedule.get("tmp").get_changes()
    shedule.get("perm").get_changes()

    bot.send_message(message.chat.id, f'Принудительная проверка выполнена!\n\n' +\
                                      message_shedule_body(shedule),
                     parse_mode='Markdown',
                     disable_web_page_preview=True)

@bot.message_handler(commands=['shedule'])
def send_shedule(message):
    bot.send_chat_action(message.chat.id, 'typing')
    bot.send_message(message.chat.id, message_shedule_body(shedule),
                     parse_mode='Markdown',
                     disable_web_page_preview=True)

@bot.message_handler(commands=['about', 'start'])
def send_about(message):
    bot.send_message(message.chat.id, f'Привет, *{message.from_user.first_name}*!\n\n' +\
                                      'Данный бот предназначен для облегчения жизни ' +\
                                      'его создателя и пользователей, показывя ' +\
                                      'дату и время публикаций файлов с постоянным и ' +\
                                      'изменениями в расписании.\n\nАвтор ' +\
                                      '*GinTR1k* (@xs\_rs), [Личный сайт](https://xs.rs/)\n' +\
                                      'Исходный код бота на [GitHub](https://g1k.me/R5Z)',
                     parse_mode='Markdown',
                     disable_web_page_preview=True)

if __name__ == "__main__":
    pool = pymysqlpool.Pool(create_instance=create_conn)

    loop = asyncio.get_event_loop()
    loop.create_task(shedule_task())

    # use webhook if enabled
    if config.webhook['enabled']:

        # Remove webhook, it fails sometimes the set if there is a previous webhook
        bot.remove_webhook()

        # Set webhook
        if config.webhook['selfsigned_cert']:
            bot.set_webhook(url=config.webhook['url_base']+config.webhook['url_path'],
                            certificate=open(config.webhook['ssl_cert'], 'r'))
        else:
            bot.set_webhook(url=config.webhook['url_base']+config.webhook['url_path'])

        # Start cherrypy server
        cherrypy.config.update({
            'server.socket_host': config.webhook['listen'],
            'server.socket_port': config.webhook['port'],
            'server.ssl_module': 'builtin',
            'server.ssl_certificate': config.webhook['ssl_cert'],
            'server.ssl_private_key': config.webhook['ssl_key']
        })

        cherrypy.config.update({'log.screen': False})
        cherrypy.quickstart(WebhookServer(), config.webhook['url_path'], {'/': {}})

    else:
        bot.infinity_polling(True)


    for task in asyncio.Task.all_tasks():
        task.cancel()

    loop.close()
