import requests

import time
from datetime import datetime, timedelta
from email.utils import parsedate

class SharagaShedule:
    def __init__(self, *, url=None, header_name=None, human_format=None):
        self.url = url
        self.header_name = header_name
        self.human_format = human_format

        self.http_response = None
        self.http_datetime = None
        self.http_error = None

        self.human_datetime = None

        self.last_check = None

    def _parse_date_HTTP(self, text):
        """ Parse HTTP date (RF-1123) to Python datetime object """

        return datetime(*parsedate(text)[:6]) + timedelta(hours=3)

    def _datetime_to_human_format(self, utc_time):
        """ Convert datetime to readeble format """

        return utc_time.strftime(self.human_format)

    def _get_HTTP_header(self, header_name):
        """ Get header's value from HTTP Response """

        return self.http_response.headers.get(header_name)

    def _get_HTTP_response(self):
        """ Make HTTP Request and get response """

        try:
            return requests.head(self.url)
        except Exception as error:
            self.http_error = error
            return None

    def get_changes(self):
        """ Make HTTP Request, get headers' data, convert HTTP date to readeble format """

        response = self._get_HTTP_response()
        if response is not None:
            self.http_response = response
            self.http_datetime = self._get_HTTP_header(self.header_name)
            self.human_datetime = self._datetime_to_human_format(self._parse_date_HTTP(self.http_datetime))

            self.http_error = None
            self.last_check = time.strftime(self.human_format)

        return self.human_datetime
